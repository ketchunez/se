#include "world.h"

class ISerializer {
    virtual void serialize(const World&) = 0;
    virtual World deserialize() = 0;
};
